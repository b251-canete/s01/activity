name = "Lhemar"
age = 26
occupation = "Inventory Analyst"
movie = "Lord Of The Rings"
rating = 99.8

print(f"I am {name}, and I am {age} years old, I work as an {occupation}, and my rating for {movie} is {rating} %")

num1 = 10
num2 = 15
num3 = 20

print(num1 * num2)
print(num1 < num3)
print(num3 + num2)